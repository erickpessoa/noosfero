# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: 1.3~rc2-1-ga15645d\n"
"PO-Revision-Date: 2015-02-23 11:37+0200\n"
"Last-Translator: Michal Čihař <michal@cihar.com>\n"
"Language-Team: Armenian <https://hosted.weblate.org/projects/noosfero/plugin-"
"custom-forms/hy/>\n"
"Language: hy\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Weblate 2.3-dev\n"

#, fuzzy
msgid "Select field"
msgstr "Նոր թղթապանակ"

#, fuzzy
msgid "Mandatory"
msgstr "Կառավարել մեկնաբանությունները"

#, fuzzy
msgid "Remove field"
msgstr "Հեռացնել բաժինը"

#, fuzzy
msgid "The %s could not be updated"
msgstr "Սկզբնական նյութի թարմացումը ձախողված է"

#, fuzzy
msgid "Access"
msgstr "Ընդունել"

#, fuzzy
msgid "The %s could not be removed"
msgstr "Սկզբնական նյութի թարմացումը ձախողված է"

#, fuzzy
msgid "Membership survey"
msgstr "Անդամներ: %s"

#, fuzzy
msgid "Triggered after membership"
msgstr "Կառավարել անդամակցությունները"

#, fuzzy
msgid "Submission could not be saved"
msgstr "Անհանատական էջի տվյալների բաժին"

#, fuzzy
msgid "There are no submissions for this form."
msgstr "%s-ի համար ենթակատեգորիաներ չկան:"

#, fuzzy
msgid "Submissions"
msgstr "Թույլտվություններ"

#, fuzzy
msgid "Pending"
msgstr "Սպասող առաջադրանքներ"

#, fuzzy
msgid "Are you sure you want to remove this query?"
msgstr "Վստա՞ք եք, որ ցականում եք այն հեռացնել:"

#, fuzzy
msgid "Type"
msgstr "Տեսակ"

#, fuzzy
msgid "Option"
msgstr "Գործողություններ"

#, fuzzy
msgid "Preselected"
msgstr "Մերժված է"

#, fuzzy
msgid "Add a new text field"
msgstr "Մեկ ձեռնարկություն"

#, fuzzy
msgid "Admission survey"
msgstr "Անդամներ: %s"

#, fuzzy
msgid "%{requestor} wants you to fill in some information before joining."
msgstr "%s ցանկանում է Ձեր ընկերը դառնալ"

#, fuzzy
msgid ""
"Before joining %{requestor}, the administrators of this organization\n"
"      wants you to fill in some further information."
msgstr "%s ցանկանում է Ձեր ընկերը դառնալ"

#, fuzzy
msgid "Manage queries"
msgstr "Կառավարել"

#, fuzzy
msgid "is mandatory."
msgstr "Պատմություն"

#, fuzzy
msgid "Add a new select field"
msgstr "Մեկ ձեռնարկություն"

#, fuzzy
msgid "Remove alternative"
msgstr "Հեռացնել անդամին"

#, fuzzy
msgid "%{requestor} wants to fill in some further information."
msgstr "%s ցանկանում է Ձեր ընկերը դառնալ"

#, fuzzy
msgid "Type:"
msgstr "Տեսակ"

#, fuzzy
msgid "%{requestor} wants you to fill in some information."
msgstr "%s ցանկանում է Ձեր ընկերը դառնալ"

#, fuzzy
msgid "Add a new alternative"
msgstr "Մեկ ձեռնարկություն"

#, fuzzy
msgid "Default text:"
msgstr "Լեզուներ"

#, fuzzy
msgid "Edit form"
msgstr "Փոփոխել"

#, fuzzy
msgid "Pending submissions for %s"
msgstr "Վիճակագրություն"

#, fuzzy
msgid "There are no pending submissions for this form."
msgstr "%s-ի համար ենթակատեգորիաներ չկան:"

#, fuzzy
msgid "Sort by"
msgstr "Մեկ համայնք"

#, fuzzy
msgid "Select"
msgstr "Որոնել..."

#, fuzzy
msgid "Back to forms"
msgstr "Արտադրանք չկա"

#, fuzzy
msgid "Logged users"
msgstr "Մուտք որպես %s"

#, fuzzy
msgid "From %s until %s"
msgstr "%-ից մինչև %"

#, fuzzy
msgid "Submission date"
msgstr "Թույլտվություններ"

#, fuzzy
msgid "Author"
msgstr "Հեղինակ"

#, fuzzy
msgid "Submissions for %s"
msgstr "Վիճակագրություն"

#, fuzzy
msgid "%{requestor} wants you to fill in some further information."
msgstr "%s ցանկանում է Ձեր ընկերը դառնալ"

#: ../controllers/custom_forms_plugin_admin_controller.rb:12
msgid "There is no data to be downloaded"
msgstr ""

#: ../controllers/custom_forms_plugin_myprofile_controller.rb:39
msgid "%s was successfully created"
msgstr ""

#: ../controllers/custom_forms_plugin_myprofile_controller.rb:64
msgid "%s was successfully updated"
msgstr ""

#: ../controllers/custom_forms_plugin_myprofile_controller.rb:78
msgid "The %s was removed"
msgstr ""

#: ../controllers/custom_forms_plugin_myprofile_controller.rb:128
msgid "Maximum file size exceeded"
msgstr ""

#: ../controllers/custom_forms_plugin_myprofile_controller.rb:182
msgid "Query Gallery"
msgstr ""

#: ../controllers/custom_forms_plugin_profile_controller.rb:33
msgid "Submission saved"
msgstr ""

#: ../controllers/custom_forms_plugin_profile_controller.rb:62
msgid "Older"
msgstr ""

#: ../controllers/custom_forms_plugin_profile_controller.rb:63
msgid "Recent"
msgstr ""

#: ../controllers/custom_forms_plugin_profile_controller.rb:65
#: ../controllers/custom_forms_plugin_profile_controller.rb:66
#: ../controllers/custom_forms_plugin_profile_controller.rb:71
#: ../lib/custom_forms_plugin/list_block.rb:7
msgid "All"
msgstr ""

#: ../controllers/custom_forms_plugin_profile_controller.rb:72
msgid "Opened"
msgstr ""

#: ../controllers/custom_forms_plugin_profile_controller.rb:73
#: ../lib/custom_forms_plugin/helper.rb:174
#: ../lib/custom_forms_plugin/helper.rb:186
#: ../lib/custom_forms_plugin/list_block.rb:9
msgid "Closed"
msgstr ""

#: ../controllers/custom_forms_plugin_profile_controller.rb:74
msgid "To come"
msgstr ""

#: ../lib/custom_forms_plugin.rb:4
msgid "Query"
msgstr ""

#: ../lib/custom_forms_plugin.rb:8
msgid "Enables the creation of custom queries like surveys or polls."
msgstr ""

#: ../lib/custom_forms_plugin.rb:20
msgid "Queries"
msgstr ""

#: ../lib/custom_forms_plugin/answer.rb:36
msgid "alternative is not valid"
msgstr ""

#: ../lib/custom_forms_plugin/answer.rb:41
msgid "do not accept multiple answers"
msgstr ""

#: ../lib/custom_forms_plugin/control_panel/all_queries.rb:6
msgid "View All"
msgstr ""

#: ../lib/custom_forms_plugin/control_panel/new_poll.rb:6
msgid "New Poll"
msgstr ""

#: ../lib/custom_forms_plugin/control_panel/new_survey.rb:6
msgid "New Survey"
msgstr ""

#: ../lib/custom_forms_plugin/csv_handler.rb:25
msgid "Timestamp"
msgstr ""

#: ../lib/custom_forms_plugin/csv_handler.rb:36
msgid " or "
msgstr ""

#: ../lib/custom_forms_plugin/csv_handler.rb:37
msgid ", or "
msgstr ""

#: ../lib/custom_forms_plugin/form.rb:34
msgid "Survey"
msgstr ""

#: ../lib/custom_forms_plugin/form.rb:35
#: ../lib/custom_forms_plugin/survey_block.rb:8
#: ../lib/custom_forms_plugin/survey_block.rb:16
msgid "Surveys"
msgstr ""

#: ../lib/custom_forms_plugin/form.rb:36
msgid "survey"
msgstr ""

#: ../lib/custom_forms_plugin/form.rb:37
msgid "surveys"
msgstr ""

#: ../lib/custom_forms_plugin/form.rb:38
msgid "Poll"
msgstr ""

#: ../lib/custom_forms_plugin/form.rb:39
#: ../lib/custom_forms_plugin/polls_block.rb:8
#: ../lib/custom_forms_plugin/polls_block.rb:16
msgid "Polls"
msgstr ""

#: ../lib/custom_forms_plugin/form.rb:40
msgid "poll"
msgstr ""

#: ../lib/custom_forms_plugin/form.rb:41
msgid "polls"
msgstr ""

#: ../lib/custom_forms_plugin/form.rb:43
msgid "%{value} is not a valid kind."
msgstr ""

#: ../lib/custom_forms_plugin/form.rb:46
#: ../views/custom_forms_plugin_myprofile/_form.html.erb:4
#: ../views/custom_forms_plugin_myprofile/_forms.html.erb:3
#: ../views/custom_forms_plugin_myprofile/_survey.html.erb:10
#: ../views/shared/_form.html.erb:6
msgid "Name"
msgstr "Անվանում"

#: ../lib/custom_forms_plugin/form.rb:47
msgid "Slug"
msgstr ""

#: ../lib/custom_forms_plugin/form.rb:48
msgid "identifier"
msgstr ""

#: ../lib/custom_forms_plugin/form.rb:49
#: ../views/custom_forms_plugin_myprofile/_form.html.erb:52
msgid "Description"
msgstr ""

#: ../lib/custom_forms_plugin/form.rb:122
msgid "This query has no ending date"
msgstr ""

#: ../lib/custom_forms_plugin/form.rb:127
msgid "Ends today"
msgstr ""

#: ../lib/custom_forms_plugin/form.rb:131
msgid "Ends tomorow"
msgstr ""

#: ../lib/custom_forms_plugin/form.rb:135
msgid "Already closed"
msgstr ""

#: ../lib/custom_forms_plugin/form.rb:138
msgid "%s days left"
msgstr ""

#: ../lib/custom_forms_plugin/form.rb:184
msgid "The time range selected is invalid."
msgstr ""

#: ../lib/custom_forms_plugin/form.rb:189
msgid "can't be less than 2"
msgstr ""

#: ../lib/custom_forms_plugin/helper.rb:19
#: ../lib/custom_forms_plugin/helper.rb:49
msgid "Always"
msgstr ""

#: ../lib/custom_forms_plugin/helper.rb:23
msgid "Until %s"
msgstr ""

#: ../lib/custom_forms_plugin/helper.rb:50
msgid "Only after the query ends"
msgstr ""

#: ../lib/custom_forms_plugin/helper.rb:51
msgid "Never"
msgstr ""

#: ../lib/custom_forms_plugin/helper.rb:64
msgid "Text field"
msgstr ""

#: ../lib/custom_forms_plugin/helper.rb:115
msgid "Hold down Ctrl to select options"
msgstr ""

#: ../lib/custom_forms_plugin/helper.rb:170
#: ../lib/custom_forms_plugin/helper.rb:178
msgid "%s left to open"
msgstr ""

#: ../lib/custom_forms_plugin/helper.rb:172
#: ../lib/custom_forms_plugin/helper.rb:184
msgid "%s left to close"
msgstr ""

#: ../lib/custom_forms_plugin/helper.rb:180
#: ../lib/custom_forms_plugin/helper.rb:189
msgid "Always open"
msgstr ""

#: ../lib/custom_forms_plugin/list_block.rb:8
msgid "Open"
msgstr ""

#: ../lib/custom_forms_plugin/list_block.rb:10
msgid "Not yet open"
msgstr ""

#: ../lib/custom_forms_plugin/list_block.rb:32
msgid "Invalid status"
msgstr ""

#: ../lib/custom_forms_plugin/membership_survey.rb:44
msgid ""
"After joining %{requestor}, the administrators of this organization\n"
"      wants you to fill in some further information."
msgstr ""

#: ../lib/custom_forms_plugin/polls_block.rb:12
msgid "Polls list in profile"
msgstr ""

#: ../lib/custom_forms_plugin/polls_block.rb:24
msgid "This block show last polls performed in profile."
msgstr ""

#: ../lib/custom_forms_plugin/select_field.rb:11
msgid "Single Choice"
msgstr ""

#: ../lib/custom_forms_plugin/select_field.rb:11
msgid "Multiple Choice"
msgstr ""

#: ../lib/custom_forms_plugin/survey_block.rb:12
msgid "Surveys list in profile"
msgstr ""

#: ../lib/custom_forms_plugin/survey_block.rb:24
msgid "This block show last surveys peformed in profile."
msgstr ""

#: ../views/blocks/_form_list_block.html.erb:1
msgid "View all"
msgstr ""

#: ../views/blocks/_form_list_block.html.erb:13
#: ../views/blocks/_queries_block.html.erb:7
msgid "No queries available"
msgstr ""

#: ../views/blocks/poll/_results.html.erb:11
msgid "See results"
msgstr ""

#: ../views/blocks/poll/_results.html.erb:23
msgid "This poll is closed."
msgstr ""

#: ../views/blocks/poll/_submission.html.erb:14
#: ../views/blocks/survey/_list_item.html.erb:30
msgid "See partial results"
msgstr ""

#: ../views/blocks/survey/_list_item.html.erb:30
msgid "See final results"
msgstr ""

#: ../views/custom_forms_plugin_admin/index.html.erb:3
msgid "Custom Forms Plugin"
msgstr ""

#: ../views/custom_forms_plugin_admin/index.html.erb:5
msgid "Export environment queries"
msgstr ""

#: ../views/custom_forms_plugin_admin/index.html.erb:8
msgid "Export one file with several queries at once."
msgstr ""

#: ../views/custom_forms_plugin_admin/index.html.erb:14
msgid "Select the user fields to be included in the submissions report"
msgstr ""

#: ../views/custom_forms_plugin_admin/index.html.erb:19
msgid "Select which profiles you want to export the queries from."
msgstr ""

#: ../views/custom_forms_plugin_admin/index.html.erb:23
msgid "Filter profiles by name"
msgstr ""

#: ../views/custom_forms_plugin_admin/index.html.erb:47
msgid "Deselect all"
msgstr ""

#: ../views/custom_forms_plugin_admin/index.html.erb:55
msgid "Generate CSV"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/_form.html.erb:8
msgid "Change picture"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/_form.html.erb:13
msgid "Remove image"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/_form.html.erb:21
#: ../views/custom_forms_plugin_myprofile/_forms.html.erb:4
msgid "Period"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/_form.html.erb:22
msgid "What is the time limit for this %s to be filled?"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/_form.html.erb:31
msgid "Who will be able to see and answer your query?"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/_form.html.erb:38
msgid "When will the results of this query be displayed?"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/_form.html.erb:46
msgid "Triggered on membership request as requirement for approval"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/_forms.html.erb:7
msgid "Actions"
msgstr "Գործողություններ"

#: ../views/custom_forms_plugin_myprofile/_forms.html.erb:11
msgid "View %s"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/_forms.html.erb:13
msgid "Review submisisons"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/_forms.html.erb:27
msgid "There are no %s yet. Why not create one now?"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/_forms.html.erb:34
msgid "Create a new %s"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/_poll.html.erb:9
msgid "Question"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/_poll.html.erb:15
msgid "Type in your question"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/_poll.html.erb:18
msgid "Multiple choice"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/_poll.html.erb:18
msgid "Checkboxes"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/_poll.html.erb:29
#: ../views/custom_forms_plugin_profile/show.html.erb:9
msgid "Remove"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/_poll.html.erb:39
#: ../views/custom_forms_plugin_myprofile/_poll.html.erb:56
msgid "Add option"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/_survey.html.erb:7
#: ../views/custom_forms_plugin_myprofile/_survey.html.erb:16
msgid "This field will be added automatically to your survey"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/_survey.html.erb:19
#: ../views/shared/_form.html.erb:7
msgid "Email"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/custom_forms_plugin/_select_field.html.erb:5
msgid "Radio"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/custom_forms_plugin/_select_field.html.erb:7
msgid "Checkbox"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/custom_forms_plugin/_select_field.html.erb:9
msgid "Drop down"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/custom_forms_plugin/_select_field.html.erb:11
msgid "Multiple Select"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/custom_forms_plugin/_select_field.html.erb:17
#: ../views/custom_forms_plugin_profile/_results_table.html.erb:5
msgid "Alternative"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/custom_forms_plugin/_text_field.html.erb:5
msgid "One-line text"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/custom_forms_plugin/_text_field.html.erb:7
msgid "Multiline text"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/custom_forms_plugin/_text_field.html.erb:13
msgid "Maximum of 255 characters"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/import.html.erb:6
msgid ""
"You must choose a CSV file that follows a specific structure. It is "
"recommended that you follow the generated template. Note that a file with "
"header is expected."
msgstr ""

#: ../views/custom_forms_plugin_myprofile/import.html.erb:11
msgid "Download Template"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/import.html.erb:16
msgid "Take note"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/import.html.erb:18
msgid ""
"Select fields are identified as single or multiple choice in the template."
msgstr ""

#: ../views/custom_forms_plugin_myprofile/import.html.erb:20
msgid ""
"A semicolon (;) should be used to separate multiple answers for multiple "
"choice fields."
msgstr ""

#: ../views/custom_forms_plugin_myprofile/import.html.erb:26
msgid "Choose a CSV file to import submissions from"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/import.html.erb:29
msgid "Maximum file size: %s"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/import.html.erb:33
#: ../views/custom_forms_plugin_myprofile/report.html.erb:65
msgid "Go back"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/import.html.erb:34
msgid "Import file"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/index.html.erb:15
#: ../views/custom_forms_plugin_profile/queries.html.erb:13
msgid "Back"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/new.html.erb:1
msgid "New %s"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/pending.html.erb:9
#: ../views/custom_forms_plugin_myprofile/pending.html.erb:14
#: ../views/custom_forms_plugin_myprofile/submissions.html.erb:11
#: ../views/custom_forms_plugin_myprofile/submissions.html.erb:22
msgid "Time"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/pending.html.erb:9
#: ../views/custom_forms_plugin_myprofile/pending.html.erb:13
#: ../views/custom_forms_plugin_profile/_text.html.erb:8
msgid "User"
msgstr "Օգտվող"

#: ../views/custom_forms_plugin_myprofile/report.html.erb:4
msgid "Report for submissions import"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/report.html.erb:7
msgid "One submission was successfully imported."
msgid_plural "%s submissions were successfully imported."
msgstr[0] ""
msgstr[1] ""

#: ../views/custom_forms_plugin_myprofile/report.html.erb:21
msgid ""
"The failed rows are listed below, and the columns with errors have been "
"highlighted. Move the cursor over the highlighted cells to see more details."
msgstr ""

#: ../views/custom_forms_plugin_myprofile/report.html.erb:31
msgid "Row"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/report.html.erb:67
msgid "Download failed entries"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/report.html.erb:69
msgid "Failed %s Entries.csv"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/report.html.erb:71
msgid "Import another file"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/show_submission.html.erb:26
msgid "Unauthenticated"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/show_submission.html.erb:49
msgid "Back to submissions"
msgstr ""

#: ../views/custom_forms_plugin_myprofile/submissions.html.erb:41
#: ../views/custom_forms_plugin_profile/_text.html.erb:30
msgid "imported submissions"
msgstr ""

#: ../views/custom_forms_plugin_profile/_queries_content.html.erb:18
msgid "%{count} submission"
msgid_plural "%{count} submissions"
msgstr[0] ""
msgstr[1] ""

#: ../views/custom_forms_plugin_profile/_queries_content.html.erb:24
msgid "Final results"
msgstr ""

#: ../views/custom_forms_plugin_profile/_queries_content.html.erb:24
msgid "Partial results"
msgstr ""

#: ../views/custom_forms_plugin_profile/_queries_content.html.erb:37
msgid "No queries found! Try different search terms or filters."
msgstr ""

#: ../views/custom_forms_plugin_profile/_results_table.html.erb:1
#: ../views/custom_forms_plugin_profile/_results_table.html.erb:2
#: ../views/custom_forms_plugin_profile/_results_table.html.erb:6
msgid "Answers"
msgstr ""

#: ../views/custom_forms_plugin_profile/_text.html.erb:9
msgid "Answer"
msgstr ""

#: ../views/custom_forms_plugin_profile/queries.html.erb:1
msgid "%s's queries"
msgstr ""

#: ../views/custom_forms_plugin_profile/review.html.erb:28
msgid "There is no answers for this question"
msgstr ""

#: ../views/custom_forms_plugin_profile/show.html.erb:7
msgid "Edit"
msgstr ""

#: ../views/custom_forms_plugin_profile/show.html.erb:24
#: ../views/tasks/custom_forms_plugin/_membership_survey_accept_details.html.erb:8
msgid "Sorry, you can't fill this form yet"
msgstr ""

#: ../views/custom_forms_plugin_profile/show.html.erb:26
#: ../views/tasks/custom_forms_plugin/_membership_survey_accept_details.html.erb:10
msgid "Sorry, you can't fill this form anymore"
msgstr ""

#: ../views/profile_design/custom_forms_plugin/_query_options.html.erb:2
msgid "%s limit"
msgstr ""

#: ../views/profile_design/custom_forms_plugin/_query_options.html.erb:4
msgid "Display %s:"
msgstr ""

#: ../views/profile_design/custom_forms_plugin/_query_options.html.erb:8
msgid "Use the filter below to display specific queries."
msgstr ""

#: ../views/profile_design/custom_forms_plugin/_query_options.html.erb:9
msgid "If only one query is chosen, it will be displayed in detail."
msgstr ""

#: ../views/profile_design/custom_forms_plugin/_query_options.html.erb:12
msgid "Find queries by name"
msgstr ""

#: ../views/shared/_form.html.erb:13
msgid "Your e-mail will be visible to this form's owners."
msgstr ""

#: ../views/shared/_form_submission.html.erb:9
msgid "This was your answer."
msgstr ""

#~ msgid "Manage Queries"
#~ msgstr "Կառավարել"

#~ msgid "Custom"
#~ msgstr "մասնակից"

#~ msgid "Custom access definitions"
#~ msgstr "մասնակից"

#~ msgid "Result Access"
#~ msgstr "Ընդունել"
